# README #

This is a very (very) simple library to aid development of data applications using Spring's JdbcTemplate. It can help bind properties to fields in circumstances where JPA or JOOQ (or other libraries) would be unnecessary.

Have a look at some of the tests to get an idea of how to use it.

## Gradle

```
repositories {
    maven {url "http://mapsum.bitbucket.org/repo/"}
}

dependencies {
    compile 'com.mapsum:kartlagt:1.0'
}
```

## Maven

```
<dependency>
    <groupId>com.mapsum</groupId>
    <artifactId>kartlagt</artifactId>
    <version>1.0</version>
</dependency>
```
