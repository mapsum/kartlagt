package com.mapsum.kartlagt;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Provides very basic mapping from a resultset to a POJO object.
 */
public class PojoMapper {

    private static final ReentrantLock LOCK = new ReentrantLock();

    private Set<Class<?>> processed;

    private Map<Class<?>, Constructor<?>> ctors;
    private Map<Class<?>, List<ColumnMap>> mappings;
    private Map<Class<?>, List<TypeConverterMap>> typeConverters;


    public PojoMapper() {
        ctors = new ConcurrentHashMap<>();
        mappings = new ConcurrentHashMap<>();
        typeConverters = new ConcurrentHashMap<>();
        processed = new HashSet<>();
    }

    /**
     * Attempts to map the given resultset to a type. Before calling this method, the ResultSet needs to have its cursor
     * on the record to be process, e.g. through {@code rset.next()}.
     * <p/>
     * This method will use the {@code @Column} annotations to determine how to map a particular class. These annotations
     * will need to be set on the various fields that need to be mapped by the mapper.
     *
     * @param rset The result set to process
     * @param type The type to map to
     * @param <T>  The class type
     * @return A new instance of the class
     * @throws ConversionException
     */
    @SuppressWarnings("unchecked")
    public <T> T map(ResultSet rset, Class<T> type) {

        if (!(ClassUtil.isTopLevelClass(type) || ClassUtil.isStaticNestedClass(type))) {
            throw new ConversionException("Cannot map type: require top level or static nested class");
        }

        if (!processed.contains(type)) {
            LOCK.lock();

            try {
                if (!processed.contains(type)) {
                    processType(type);
                }

            } finally {
                LOCK.unlock();
            }
        }

        Constructor<T> ctor = (Constructor<T>) ctors.get(type);
        List<ColumnMap> map = mappings.get(type);
        List<TypeConverterMap> convs = typeConverters.get(type);

        T instance;

        try {
            instance = ctor.newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new ConversionException("Failed to created new instance of type: " + type, e);
        }

        Set<String> names;

        try {
            names = getColumnNames(rset);
        } catch (SQLException e) {
            throw new ConversionException("Failed to map result set", e);
        }

        for (ColumnMap cmap : map) {

            try {

                if (!names.contains(cmap.column)) {
                    continue;
                }

                if (!cmap.nullable && (rset.getObject(cmap.column) == null || rset.wasNull())) {
                    throw new ConversionException("Column '" + cmap.column + "' was null but field not declared as nullable '" + cmap.field.toGenericString() + "'");
                }

                if (List.class.isAssignableFrom(cmap.field.getType())) {
                    if (rset.getMetaData().getColumnType(rset.findColumn(cmap.column)) != Types.ARRAY) {
                        throw new ConversionException("Cannot bind List field '" + cmap.field.toGenericString() + "': result set type is not an array");
                    }

                    Object[] vals = (Object[]) rset.getArray(cmap.column).getArray();

                    if (vals == null) {
                        cmap.field.set(instance, null);
                        continue;
                    }

                    if (vals.length == 0) {
                        cmap.field.set(instance, Collections.emptyList());
                        continue;
                    }

                    Object[] container = new Object[vals.length];

                    if (cmap.type != null && cmap.type.isEnum()) {
                        for (int i = 0; i < vals.length; i++) {
                            container[i] = toEnum(vals[i], (Class<Enum>) cmap.type);
                        }
                    }

                    cmap.field.set(instance, Arrays.asList(container));

                    continue;
                }

                if (Set.class.isAssignableFrom(cmap.field.getType())) {
                    if (rset.getMetaData().getColumnType(rset.findColumn(cmap.column)) != Types.ARRAY) {
                        throw new ConversionException("Cannot bind Set field '" + cmap.field.toGenericString() + "': result set type is not an array");
                    }

                    Object[] vals = (Object[]) rset.getArray(cmap.column).getArray();

                    if (vals == null) {
                        cmap.field.set(instance, null);
                        continue;
                    }

                    if (vals.length == 0) {
                        cmap.field.set(instance, Collections.emptySet());
                        continue;
                    }

                    Object[] container = new Object[vals.length];

                    if (cmap.type != null && cmap.type.isEnum()) {
                        for (int i = 0; i < vals.length; i++) {
                            container[i] = toEnum(vals[i], (Class<Enum>) cmap.type);
                        }
                    }

                    cmap.field.set(instance, new HashSet<>(Arrays.asList(container)));

                    continue;
                }

                if (cmap.field.getType().isEnum()) {
                    Enum<?> val = toEnum(rset.getObject(cmap.column), (Class<Enum>) cmap.field.getType());
                    cmap.field.set(instance, val);
                    continue;
                }

                if (URL.class.isAssignableFrom(cmap.field.getType())) {
                    Object val = rset.getObject(cmap.column);

                    if (val instanceof String) {
                        try {
                            cmap.field.set(instance, new URL((String) val));
                        } catch (MalformedURLException e) {
                            throw new ConversionException("Cannot bind URL: invalid URL string", e);
                        }
                    } else if (val instanceof URL) {
                        cmap.field.set(instance, val);
                    } else {
                        throw new ConversionException("Cannot bind URL: unexpected database value " + val.getClass());
                    }

                    continue;
                }

                cmap.field.set(instance, rset.getObject(cmap.column));

            } catch (SQLException | IllegalAccessException | IllegalArgumentException e) {
                throw new ConversionException("Failed to map column: " + cmap, e);
            }
        }

        for (TypeConverterMap vmap : convs) {
            try {
                vmap.field.set(instance, vmap.converter.convert(rset.getObject(vmap.column)));
            } catch (SQLException | IllegalAccessException | IllegalArgumentException e) {
                throw new ConversionException("Failed to map column: " + vmap, e);
            }
        }

        return instance;
    }

    private synchronized void processType(Class<?> base) {

        Class<?> type = base;
        ctors.put(base, getDefaultConstructor(base));

        List<ColumnMap> cmaps = new ArrayList<>();
        List<TypeConverterMap> tmaps = new ArrayList<>();

        Column col;
        UDT convert;

        do {
            for (Field f : base.getDeclaredFields()) {
                col = f.getAnnotation(Column.class);

                if (col == null) {
                    continue;
                }

                if (!f.isAccessible()) {
                    f.setAccessible(true);
                }

                String column = col.name();

                if (column == null || column.trim().length() == 0) {
                    column = f.getName().toLowerCase();
                } else {
                    column = column.toLowerCase();
                }

                convert = f.getAnnotation(UDT.class);

                if (convert == null) {
                    cmaps.add(new ColumnMap(column, base, f, col.nullable(), col.type()));
                    continue;
                }

                try {
                    tmaps.add(new TypeConverterMap(
                        column,
                        getDefaultConstructor(convert.using()).newInstance(),
                        f,
                        col.nullable()
                    ));
                } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    throw new ConversionException("Failed to created new instance of type: " + convert.using(), e);
                }


            }

            base = base.getSuperclass();
        } while (base != null && !Object.class.equals(base));

        mappings.put(type, cmaps);
        typeConverters.put(type, tmaps);
        processed.add(base);


    }

    /**
     * Fetches the column names for a resultset
     *
     * @param rset The resultset to process
     * @return The set of column names. Note that duplicate column names will be ignored.
     * @throws SQLException
     */
    private static Set<String> getColumnNames(ResultSet rset) throws SQLException {
        ResultSetMetaData meta = rset.getMetaData();
        int numCol = meta.getColumnCount();
        HashSet<String> names = new HashSet<>();

        for (int i = 1; i <= numCol; i++) {
            names.add(meta.getColumnName(i).toLowerCase());
        }
        return names;
    }

    /**
     * Attempts to locate the default, no-arg constructor for a particular class.
     *
     * @param type The class to interrogate
     * @param <T>  The class type
     * @return The default c'tor
     * @throws ConversionException If no constructor can be found for the type.
     */
    @SuppressWarnings("unchecked")
    private static <T> Constructor<T> getDefaultConstructor(Class<T> type) {
        for (Constructor<?> c : type.getDeclaredConstructors()) {
            if (c.getParameterTypes().length == 0) {
                c.setAccessible(true);
                return (Constructor<T>) c;
            }
        }

        for (Constructor<?> c : type.getConstructors()) {
            if (c.getParameterTypes().length == 0) {
                c.setAccessible(true);
                return (Constructor<T>) c;
            }
        }

        throw new ConversionException("No default constructor found for type: " + type);

    }

    /**
     * Attempts to map the given type to an enum. The given type will need to be one of Integer/Long/String corresponding
     * to the enum ordinals or name.
     *
     * @param val  The value to query
     * @param type The enum type
     * @return The enum value
     * @throws ConversionException If the given val type is not supported or the value could not be
     *                             converted to an enum
     */
    private static Enum toEnum(Object val, Class<Enum> type) {
        if (val instanceof Integer) {
            return type.getEnumConstants()[(Integer) val];
        }

        if (val instanceof Long) {
            return type.getEnumConstants()[((Long) val).intValue()];
        }

        if (val instanceof String) {
            return Enum.valueOf(type, (String) val);
        }

        throw new ConversionException("Cannot bind to enum: database value must be integer or string");


    }


}
