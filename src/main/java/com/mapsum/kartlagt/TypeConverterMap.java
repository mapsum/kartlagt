package com.mapsum.kartlagt;

import java.lang.reflect.Field;

class TypeConverterMap {
    public final TypeConverter converter;
    public final Field field;
    public final boolean nullable;
    public final String column;

    TypeConverterMap(String column, TypeConverter converter, Field field, boolean nullable) {
        this.converter = converter;
        this.field = field;
        this.column = column;
        this.nullable = nullable;

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TypeConverterMap{");
        sb.append("using=").append(converter);
        sb.append(", field=").append(field);
        sb.append(", nullable=").append(nullable);
        sb.append(", column='").append(column).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
