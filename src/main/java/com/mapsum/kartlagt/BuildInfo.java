package com.mapsum.kartlagt;

public class BuildInfo {

    public static final String SHA = "9d63950e5609969ec87154dded5c452ca2bdff78";
    public static final String DATE = "2015-02-08T13:48:53.641Z";
    public static final String GROUP = "com.mapsum";
    public static final String NAME = "kartlagt";
    public static final String VER = "1.0";
    public static final String FULL_VERSION = String.format("%s%n%s%n%s:%s:%s", SHA, DATE, GROUP, NAME, VER);

    public static final boolean DEVELOPMENT = false;


}