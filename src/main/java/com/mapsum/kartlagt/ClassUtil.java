package com.mapsum.kartlagt;

import java.lang.reflect.Modifier;

public class ClassUtil {

    public static boolean isTopLevelClass(Class<?> type) {
        return !(type.isAnonymousClass() || type.isLocalClass() || type.isMemberClass());
    }

    public static boolean isStaticNestedClass(Class<?> type) {
        return type.isMemberClass() && Modifier.isStatic(type.getModifiers());
    }

    public static boolean isInnerClass(Class<?> type) {

        return type.isAnonymousClass() ||
            type.isLocalClass() ||
            (type.isMemberClass() && !Modifier.isStatic(type.getModifiers()));
    }

    public boolean isNestedClass(Class<?> type) {
        return !isTopLevelClass(type);
    }
}
