package com.mapsum.kartlagt;


import java.lang.reflect.Field;

class ColumnMap {
    public final String column;
    public final Class<?> clazz;
    public final Field field;
    public final boolean nullable;
    public final Class<?> type;

    ColumnMap(String column, Class<?> clazz, Field field, boolean nullable, Class<?> type) {
        this.column = column;
        this.clazz = clazz;
        this.field = field;
        this.nullable = nullable;
        this.type = type;
    }

    @Override
    public String toString() {
        return "ColumnMap{" +
            "column='" + column + '\'' +
            ", clazz=" + clazz +
            ", field=" + field +
            ", nullable=" + nullable +
            ", type=" + type +
            '}';
    }
}
