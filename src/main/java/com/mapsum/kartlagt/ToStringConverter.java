package com.mapsum.kartlagt;


public class ToStringConverter implements TypeConverter {
    @Override
    @SuppressWarnings("unchecked")
    public String convert(Object in) throws ConversionException {

        if (in == null) {
            return null;
        }

        return in.toString();
    }
}
