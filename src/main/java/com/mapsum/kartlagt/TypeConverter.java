package com.mapsum.kartlagt;


/**
 * Handles specific conversion of column data to a type
 */
public interface TypeConverter {

    <T> T convert(Object in) throws ConversionException;
}
